import React, { Fragment } from 'react'
import { httpClient } from '../utils/httpClient'

const getAllBlogPost = (params: any) => {
	return httpClient({
		url: `/PostData/Search`,
		method: 'GET',
		params,
	})
}

const updateBlogPost = (data: any) => {
	return httpClient({
		url: '/PostData/Update',
		method: 'PATCH',
		isAuth: true,
		data,
	})
}

const updateCategory = (data: any) => {
	return httpClient({
		url: '/CategoryData/Update',
		method: 'PATCH',
		isAuth: true,
		data,
	})
}

const AsyncMutex = () => {
	const fetchData = async () => {
		await updateBlogPost({
			dataKey: '77fe36553ec141538c13bd2c5d94e881',
			categoryDataKey: '2d5d7e20ea334003893015c793b4006c',
			category: 'Hợp tác cùng Việt An',
			categoryUrl: 'Hop-tac-cung-Viet-An',
			type: 'NORMAL',
			title: 'Đây là bài viết test',
			thumb: '',
			description: 'Mô tả bài viết test',
			body: '',
			url: 'Day-la-bai-viet-test',
			status: 'ACTIVE',
			createDate: '2023-05-27T12:06:18.843',
		})

		await getAllBlogPost({
			pageSize: 10,
			pageIndex: 1,
		})

		await updateCategory({
			dataKey: '2d5d7e20ea334003893015c793b4006c',
			name: 'Hợp tác cùng Việt An',
			description: '',
			thumb: '',
			url: 'Hop-tac-cung-Viet-An',
			body: '1',
			createDate: '2023-04-18T13:38:49.297',
			status: 'ACTIVE',
		})
	}

	return (
		<Fragment>
			{/* <div style={{ width: '50vw', height: '200px', overflow: 'auto' }}>{JSON.stringify(data)}</div> */}

			<button onClick={fetchData}>Fetch Data</button>
		</Fragment>
	)
}

export default AsyncMutex
