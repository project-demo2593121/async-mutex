import React, { useState } from 'react'
import { Mutex, MutexInterface } from 'async-mutex'

const mutex: MutexInterface = new Mutex()

function Counter() {
	const [count, setCount] = useState(0)

	async function increment() {
		console.log('increment')

		const release = await mutex.acquire()

		try {
			await setTimeout(() => {
				console.log('fetch api')
				setCount((prev) => prev + 1)

				if (count > 5) throw Error('loi o day ne!')
				release()
			}, 2000)
		} catch (error) {
			console.log({ error })
		} finally {
			console.log('finally')
		}
	}

	return (
		<div>
			<p>Count: {count}</p>
			<button onClick={increment}>Increment</button>
		</div>
	)
}

export default Counter
