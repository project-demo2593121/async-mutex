/* eslint-disable no-useless-catch */
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig } from 'axios'
import { Mutex } from 'async-mutex'
import { token } from './constant'

import Cookies from 'js-cookie'

type THttpClientProps = {
	url: string
	method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
	isAuth?: boolean
	customHeader?: any
	data?: Record<string, any> | undefined
	params?: Record<string, any> | undefined
}

const baseURL = 'https://wap.787.vn'

const mutex = new Mutex()

export const httpClient = <ReturnType>(config: THttpClientProps) => {
	const { url, method, isAuth, customHeader, data, params } = config

	const headers: { [key: string]: string } = {}
	headers['Content-Type'] = 'application/json'

	const axiosConfig: AxiosRequestConfig = {
		baseURL,
		url,
		method,
		...(customHeader ?? {
			headers: {
				...headers,
			},
		}),
		...(params && { params }),
		...(data && { data }),
	}

	const Axios = axios.create(axiosConfig)

	Axios.interceptors.request.use(
		async (config: InternalAxiosRequestConfig) => {
			let release = null

			if (isAuth) release = await mutex.acquire()

			try {
				const accessToken = Cookies.get('accessToken')

				// kiểm tra token
				if (accessToken) {
					config.headers.Authorization = `Bearer ${accessToken}`
				} else {
					// call api refresh token -> token và refresh token mới
					const result = await new Promise((resolve, reject) => {
						setTimeout(() => {
							resolve('token')
						}, 1000)
					})

					Cookies.set('accessToken', result as string)
					config.headers.Authorization = `Bearer ${result}`
				}

				release && release()
				return config
			} catch (error) {
				release && release()
				throw error
			}
		},
		(error: AxiosError) => {
			return Promise.reject(error)
		}
	)
	Axios.interceptors.response.use(
		(response: AxiosResponse) => {
			return response
		},
		async (error: AxiosError) => {
			const originalRequest = error.config as InternalAxiosRequestConfig

			if (error.response?.status === 401) {
				const release = await mutex.acquire()
				try {
					// call api refresh token -> token và refresh token mới
					const result = await new Promise((resolve, reject) => {
						setTimeout(() => {
							resolve(token)
						}, 1000)
					})

					Cookies.set('accessToken', result as string)
					Axios.defaults.headers.common['Authorization'] = `Bearer ${result}`

					release()
					return Axios(originalRequest)
				} catch (error) {
					release()
					console.log('window.location = /login')
				}
			}

			return Promise.reject(error)
		}
	)

	return new Promise<ReturnType>((resolve, reject) =>
		Axios(axiosConfig)
			.then((response) => resolve(response.data))
			.catch((e: AxiosError) => reject(e))
	)
}
