import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Counter from './components/Couter'
import AsyncMutex from './components/AsyncMutex'
import { token } from './utils/constant'
import Cookies from 'js-cookie'

function App() {
	const [count, setCount] = useState(0)

	return (
		<>
			<div>
				<a href='https://vitejs.dev' target='_blank'>
					<img src={viteLogo} className='logo' alt='Vite logo' />
				</a>
				<a href='https://react.dev' target='_blank'>
					<img src={reactLogo} className='logo react' alt='React logo' />
				</a>
			</div>
			<h1>Vite + React</h1>
			<div className='card'>
				{/* <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button> */}

				{/* <Counter /> */}

				<button
					onClick={() => {
						const accessToken = Cookies.get('accessToken')
						console.log('💕 ~ file: App.tsx:33 ~ App ~ accessToken:', accessToken)
					}}
				>
					Show accessToken
				</button>

				<button
					onClick={() => {
						Cookies.set('accessToken', token)
					}}
				>
					Set accessToken
				</button>

				<button
					onClick={() => {
						Cookies.remove('accessToken')
					}}
				>
					Remove accessToken
				</button>

				<AsyncMutex />

				<p>
					Edit <code>src/App.tsx</code> and save to test HMR
				</p>
			</div>
			<p className='read-the-docs'>Click on the Vite and React logos to learn more</p>
		</>
	)
}

export default App
